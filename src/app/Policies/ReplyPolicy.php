<?php


namespace App\Policies;

use App\Models\Reply;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReplyPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Post $reply
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function replyAuthor(User $user, Reply $reply)
    {
        return $user->id === $reply->user_id;
    }


}
