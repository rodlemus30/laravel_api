<?php

namespace App\Services\Auth;

use App\Repositories\UserRepo\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;
use InvalidArgumentException;

class AuthService
{


    protected $_userRepository;


    public function __construct(UserRepository $userRepo)
    {
        $this->_userRepository = $userRepo;
    }


    public function login($data, Request $request): array
    {


        $validator = Validator::make($request->all(), ['email' => 'required', 'password' => 'required']);


        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        $userDB = $this->_userRepository->getUserByEmail($data['email']);
        if (!$userDB || !Hash::check($data['password'], $userDB['password'])) {
            throw new UnauthorizedException('Email or Password Incorrect');
        }

        $token = $userDB->createToken('access_token')->accessToken;

        return ['user' => $userDB, 'access_token' => $token];
    }
}
