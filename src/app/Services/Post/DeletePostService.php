<?php


namespace App\Services\Post;


use App\Repositories\Post\PostRepository;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\UnauthorizedException;

class DeletePostService
{

    protected $_postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->_postRepository = $postRepository;
    }


    public function deletePostById($post_id)
    {
        $post = $this->_postRepository->findPostById($post_id);
        try {
            Gate::authorize('postAuthor', $post);
        } catch (AuthorizationException $e) {
            throw new UnauthorizedException();
        }
        return $this->_postRepository->deletePostById($post_id);
    }
}
