<?php


namespace App\Services\Post;


use App\Repositories\Post\PostRepository;
use Illuminate\Http\Request;

class GetPostService
{

    protected $_postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->_postRepository = $postRepository;
    }

    public function getPostsByUserId(Request $request, $user_id)
    {
        $pagination = $request->query('pagination');
        $pagination = is_null($pagination) || $pagination > 10 || $pagination < 1 ? 5 : (int)$pagination;

        return $this->_postRepository->getPostsByUserId($user_id, $pagination);

    }
}
