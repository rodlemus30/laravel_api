<?php


namespace App\Services\Post;


use App\Repositories\Post\PostRepository;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;
use InvalidArgumentException;

class UpdatePostService
{

    protected $_postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->_postRepository = $postRepository;
    }

    public function updatePost($data, $post_id, Request $request)
    {

        $post = $this->_postRepository->findPostById($post_id);

        try {
            Gate::authorize('postAuthor', $post);
        } catch (AuthorizationException $e) {
            throw new UnauthorizedException();
        }

        $validator = Validator::make($request->all(), ['title' => 'required', 'content' => 'required']);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }


        $post->title = $data['title'];
        $post->content = $data['content'];

        return $post->save();

    }
}
