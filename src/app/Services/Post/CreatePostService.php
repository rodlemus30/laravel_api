<?php


namespace App\Services\Post;


use App\Repositories\Post\PostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class CreatePostService
{
    protected $_postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->_postRepository = $postRepository;
    }

    public function createPost($data, Request $request)
    {
        $validator = Validator::make($request->all(), ['title' => 'required', 'content' => 'required']);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }


        return $this->_postRepository->savePost($data, $request->user()->user_id);

    }
}
