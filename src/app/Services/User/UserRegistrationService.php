<?php

namespace App\Services\User;


use App\Repositories\UserRepo\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;


class UserRegistrationService
{
    protected $_userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->_userRepository = $userRepository;
    }


    public function createUser($data, Request $request)
    {
        $validator = Validator::make($request->all(), ['email' => 'required|unique:users,email', 'password' => 'required|string', 'name' => 'required|string']);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        $avatarUrl = $this->generateAvatar($data['email']);

        $encryptedData = ['email' => $data['email'], 'avatar' => $avatarUrl, 'password' => Hash::make($data['password']), 'name' => $data['name']];

        return $this->_userRepository->saveUser($encryptedData);
    }

    private function generateAvatar($email): string
    {
        $hashedEmail = md5(strtolower(trim($email)));
        return 'https://www.gravatar.com/avatar/' . $hashedEmail;
    }

}
