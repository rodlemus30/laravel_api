<?php


namespace App\Services\Reply;


use App\Repositories\Reply\ReplyRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class CreateReplyService
{

    protected $_replyRepository;

    public function __construct(ReplyRepository $replyRepository)
    {
        $this->_replyRepository = $replyRepository;
    }

    public function createReplyFromPost(Request $request, $data, $post_id)
    {

        $validator = Validator::make($request->all(), ['content' => 'required']);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        return $this->_replyRepository->saveReplyFromPost($data, $request->user(), $post_id);


    }

    public function createReplyFromReply(Request $request, $data)
    {

        $validator = Validator::make($request->all(), ['content' => 'required', 'post_id' => 'required', 'reply_parent_id' => 'required']);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        ['post_id' => $post_id, 'reply_parent_id' => $reply_parent_id, 'content' => $content] = $data;

        return $this->_replyRepository->saveReplyFromReply($content, $request->user(), $reply_parent_id, $post_id);
    }
}
