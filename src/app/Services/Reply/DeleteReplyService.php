<?php


namespace App\Services\Reply;


use App\Repositories\Reply\ReplyRepository;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;

class DeleteReplyService
{
    protected $_replyRepository;

    public function __construct(ReplyRepository $replyRepository)
    {
        $this->_replyRepository = $replyRepository;
    }

    public function deleteReplyById($reply_id)
    {
        $reply = $this->_replyRepository->findReplyById($reply_id);
        try {
            Gate::authorize('replyAuthor', $reply);
        } catch (AuthorizationException $e) {
            throw new UnauthorizedException();
        }
        return $this->_replyRepository->deleteReplyById($reply_id);
    }
}
