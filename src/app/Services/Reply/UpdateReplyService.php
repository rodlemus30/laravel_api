<?php


namespace App\Services\Reply;


use App\Repositories\Reply\ReplyRepository;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;
use InvalidArgumentException;

class UpdateReplyService
{
    protected $_replyRepository;

    public function __construct(ReplyRepository $replyRepository)
    {
        $this->_replyRepository = $replyRepository;
    }

    public function updateReplyById($data, $reply_id, Request $request)
    {
        $reply = $this->_replyRepository->findReplyById($reply_id);
        try {
            Gate::authorize('replyAuthor', $reply);
        } catch (AuthorizationException $e) {
            throw new UnauthorizedException();
        }

        $validator = Validator::make($request->all(), ['content' => 'required']);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        $reply->content = $data['content'];

        return $reply->save();
    }
}
