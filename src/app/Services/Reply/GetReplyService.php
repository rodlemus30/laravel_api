<?php


namespace App\Services\Reply;


use App\Repositories\Reply\ReplyRepository;
use Illuminate\Support\Collection;

class GetReplyService
{
    protected $_replyRepository;

    public function __construct(ReplyRepository $replyRepository)
    {
        $this->_replyRepository = $replyRepository;
    }

    public function getPrincipalRepliesByPostId($post_id): Collection
    {
        return $this->_replyRepository->getPrincipalRepliesByPostId($post_id)->collect()->map(function ($reply) {
            $numOfRepliesOfEachPrincipalReply = $this->_replyRepository->getChildsRepliesByReplyId($reply->reply_id)->count();
            return $reply->setAttribute('numOfReplies', $numOfRepliesOfEachPrincipalReply);
        });
    }

    public function getChildRepliesByReplyId($reply_id)
    {
        return $this->_replyRepository->getChildsRepliesByReplyId($reply_id);
    }
}
