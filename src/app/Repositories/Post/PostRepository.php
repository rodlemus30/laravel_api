<?php


namespace App\Repositories\Post;


use App\Models\Post;
use App\Models\User;

class PostRepository
{

    protected $_postRepository;
    protected $_userRepository;

    public function __construct(Post $post, User $userRepo)
    {
        $this->_postRepository = $post;
        $this->_userRepository = $userRepo;
    }

    public function savePost($post, $userId)
    {
        $newPost = new Post($post);
        $user = $this->_userRepository->newQuery()->find($userId);
        return $user->posts()->save($newPost);
    }

    public function findPostById($post_id)
    {
        return $this->_postRepository->newQuery()->findOrFail($post_id);
    }

    public function deletePostById($post_id)
    {
        return $this->_postRepository->newQuery()->findOrFail($post_id)->delete();
    }

    public function getPostsByUserId($user_id, $pagination)
    {
        return $this->_postRepository->newQuery()->where('user_id', '=', $user_id)->paginate($pagination);
    }
}
