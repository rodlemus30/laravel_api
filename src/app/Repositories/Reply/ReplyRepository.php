<?php


namespace App\Repositories\Reply;


use App\Models\Reply;
use App\Repositories\Post\PostRepository;

class ReplyRepository
{
    protected $_replyRepository;
    protected $_postRepository;

    public function __construct(Reply $replyRepository, PostRepository $postRepository)
    {
        $this->_replyRepository = $replyRepository;
        $this->_postRepository = $postRepository;
    }


    public function saveReplyFromReply($content, $user, $reply_parent_id, $post_id)
    {
        $newReply = $this->_replyRepository->make(['content' => $content]);
        $newReply->users()->associate($user);
        $parentReply = $this->_replyRepository->newQuery()->findOrFail($reply_parent_id);
        $post = $this->_postRepository->findPostById($post_id);
        $newReply->posts()->associate($post);


        return $parentReply->replies()->save($newReply);

    }

    public function saveReplyFromPost($reply, $user, $post_id)
    {
        $newReply = $this->_replyRepository->make($reply);
        $newReply->users()->associate($user);
        $post = $this->_postRepository->findPostById($post_id);
        return $post->replies()->save($newReply);
    }

    public function deleteReplyById($reply_id)
    {
        return $this->_replyRepository->newQuery()->findOrFail($reply_id)->delete();
    }

    public function findReplyById($reply_id)
    {
        return $this->_replyRepository->newQuery()->findOrFail($reply_id);
    }


    public function getChildsRepliesByReplyId($reply_id)
    {
        return $this->_replyRepository->newQuery()->where('reply_parent_id', '=', $reply_id)->get();
    }

    public function getPrincipalRepliesByPostId($post_id)
    {
        return $this->_replyRepository->newQuery()->where('post_id', '=', $post_id)->where('reply_parent_id', '=', null)->get();
    }
}
