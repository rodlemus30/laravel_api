<?php

namespace App\Repositories\UserRepo;

use App\Models\User;


class UserRepository
{

    protected $_userModel;

    public function __construct(User $user)
    {
        $this->_userModel = $user;
    }

    public function getUserByEmail($userEmail)
    {

        return $this->_userModel->newQuery()->where('email', $userEmail)->first();
    }

    public function saveUser($user)
    {
        return $this->_userModel->newQuery()->create($user);
    }
}
