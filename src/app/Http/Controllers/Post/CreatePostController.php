<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Services\Post\CreatePostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use InvalidArgumentException;


class CreatePostController extends Controller
{

    protected $_postRegisterService;

    public function __construct(CreatePostService $createPostService)
    {
        $this->_postRegisterService = $createPostService;
    }


    public function createPost(Request $request): JsonResponse
    {
        $data = $request->only(['title', 'content']);
        $result = ['status' => 200];

        try {
            $result['data'] = $this->_postRegisterService->createPost($data, $request);
        } catch (\Exception $error) {

            if ($error instanceof InvalidArgumentException) {
                $result403 = ['status' => 403, 'error' => $error->getMessage()];
                return response()->json($result403, $result403['status']);
            }
            $result = ['status' => 500, 'error' => $error->getMessage()];
        }
        return response()->json($result, $result['status']);
    }

}
