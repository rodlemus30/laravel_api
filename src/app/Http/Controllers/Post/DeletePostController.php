<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Services\Post\DeletePostService;
use Illuminate\Validation\UnauthorizedException;

class DeletePostController extends Controller
{
    protected $_deletePostService;

    public function __construct(DeletePostService $deletePostService)
    {
        $this->_deletePostService = $deletePostService;
    }

    public function deletePost($post_id)
    {
        $result = ['status' => 204];

        try {
            $result['data'] = $this->_deletePostService->deletePostById($post_id);
        } catch (\Exception $e) {

            if ($e instanceof UnauthorizedException) {
                $result401 = ['status' => 401, 'error' => 'Unauthorized Operation'];
                return response()->json($result401, $result401['status']);
            }

            $result = ['status' => 500, 'error' => $e->getMessage()];
        }

        return response()->json($result, $result['status']);
    }
}
