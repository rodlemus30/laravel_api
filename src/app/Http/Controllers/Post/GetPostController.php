<?php


namespace App\Http\Controllers\Post;


use App\Http\Controllers\Controller;
use App\Services\Post\GetPostService;
use Illuminate\Http\Request;

class GetPostController extends Controller
{
    protected $_getPostService;

    public function __construct(GetPostService $getPostService)
    {
        $this->_getPostService = $getPostService;
    }

    public function getPostsByUserId(Request $request, $user_id)
    {
        return $this->_getPostService->getPostsByUserId($request, $user_id);
    }
}
