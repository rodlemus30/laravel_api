<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Services\Post\UpdatePostService;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class UpdatePostController extends Controller
{
    protected $_updatePostService;

    public function __construct(UpdatePostService $updatePostService)
    {
        $this->_updatePostService = $updatePostService;
    }

    public function updatePost(Request $request, $post_id)
    {
        $data = $request->only(['title', 'content']);
        $result = ['status' => 200];

        try {
            $result['data'] = $this->_updatePostService->updatePost($data, $post_id, $request);
        } catch (\Exception $error) {

            if ($error instanceof UnauthorizedException) {
                $result403 = ['status' => 403, 'error' => 'Unauthorized Operation'];
                return response()->json($result403, $result403['status']);
            }
            $result = ['status' => 500, 'error' => $error->getMessage()];
        }
        return response()->json($result, $result['status']);

    }
}
