<?php

namespace App\Http\Controllers;

use App\Services\User\UserRegistrationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserRegistrationController extends Controller
{

    protected $_userService;

    public function __construct(UserRegistrationService $userService)
    {
        $this->_userService = $userService;
    }

    public function createUser(Request $request): JsonResponse
    {
        $data = $request->only(['name', 'email', 'password']);
        $result = ['status' => 200];

        try {
            $result['data'] = $this->_userService->createUser($data, $request);
        } catch (\Exception $error) {

            if ($error instanceof \InvalidArgumentException) {
                $result403 = ['status' => 403, 'error' => $error->getMessage()];
                return response()->json($result403, $result403['status']);
            }
            $result = ['status' => 500, 'error' => $error->getMessage()];
        }

        return response()->json($result, $result['status']);
    }
}
