<?php

namespace App\Http\Controllers;


use App\Services\Auth\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class AuthController extends Controller
{


    protected $_authService;

    public function __construct(AuthService $authService)
    {
        $this->_authService = $authService;
    }


    public function login(Request $request): JsonResponse
    {
        $data = $request->only(['email', 'password']);
        $result = ['status' => 200];

        try {
            $result['data'] = $this->_authService->login($data, $request);
        } catch (\Exception $error) {

            if ($error instanceof UnauthorizedException) {
                $result401 = ['status' => 401, 'error' => $error->getMessage()];
                return response()->json($result401, $result401['status']);
            }

            $result = ['status' => 500, 'error' => $error->getMessage()];
        }

        return response()->json($result, $result['status']);


    }
}
