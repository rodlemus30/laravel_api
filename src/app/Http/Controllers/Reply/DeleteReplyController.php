<?php


namespace App\Http\Controllers\Reply;


use App\Http\Controllers\Controller;
use App\Services\Reply\DeleteReplyService;
use Illuminate\Validation\UnauthorizedException;

class DeleteReplyController extends Controller
{
    protected $_deleteReplyService;

    public function __construct(DeleteReplyService $deleteReplyService)
    {
        $this->_deleteReplyService = $deleteReplyService;
    }

    public function deleteReplyById($reply_id)
    {
        $result = ['status' => 204];

        try {
            $result['data'] = $this->_deleteReplyService->deleteReplyById($reply_id);
        } catch (\Exception $e) {

            if ($e instanceof UnauthorizedException) {
                $result401 = ['status' => 401, 'error' => 'Unauthorized Operation'];
                return response()->json($result401, $result401['status']);
            }

            $result = ['status' => 500, 'error' => $e->getMessage()];
        }

        return response()->json($result, $result['status']);
    }
}
