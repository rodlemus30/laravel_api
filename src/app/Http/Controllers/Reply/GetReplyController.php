<?php


namespace App\Http\Controllers\Reply;


use App\Http\Controllers\Controller;
use App\Services\Reply\GetReplyService;
use Exception;
use InvalidArgumentException;

class GetReplyController extends Controller
{
    protected $_getReplyService;

    public function __construct(GetReplyService $getReplyService)
    {
        $this->_getReplyService = $getReplyService;
    }

    public function getPrincipalRepliesByPostId($post_id)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->_getReplyService->getPrincipalRepliesByPostId($post_id);
        } catch (Exception $error) {
            if ($error instanceof InvalidArgumentException) {
                $result403 = ['status' => 403, 'error' => $error->getMessage()];
                return response()->json($result403, $result403['status']);
            }
            $result = ['status' => 500, 'error' => $error->getMessage()];
        }
        return response()->json($result, $result['status']);
    }

    public function getChildRepliesByReplyId($reply_id)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->_getReplyService->getChildRepliesByReplyId($reply_id);
        } catch (Exception $error) {
            if ($error instanceof InvalidArgumentException) {
                $result403 = ['status' => 403, 'error' => $error->getMessage()];
                return response()->json($result403, $result403['status']);
            }
            $result = ['status' => 500, 'error' => $error->getMessage()];
        }
        return response()->json($result, $result['status']);
    }
}
