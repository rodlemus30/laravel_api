<?php

namespace App\Http\Controllers\Reply;

use App\Http\Controllers\Controller;
use App\Services\Reply\CreateReplyService;
use Illuminate\Http\Request;

class CreateReplyController extends Controller
{
    protected $_createReplyService;

    public function __construct(CreateReplyService $createReplyService)
    {
        $this->_createReplyService = $createReplyService;
    }

    public function createReplyFromPost(Request $request, $post_id)
    {
        $data = $request->only(['title', 'content']);
        $result = ['status' => 200];

        $result['data'] = $this->_createReplyService->createReplyFromPost($request, $data, $post_id);

        return response()->json($data, $result['status']);

    }

    public function createReplyFromReply(Request $request)
    {
        $data = $request->only(['title', 'content', 'post_id', 'reply_parent_id']);
        $result = ['status' => 200];

        $result['data'] = $this->_createReplyService->createReplyFromReply($request, $data);

        return response()->json($data, $result['status']);

    }
}
