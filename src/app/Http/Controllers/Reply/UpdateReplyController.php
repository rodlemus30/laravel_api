<?php


namespace App\Http\Controllers\Reply;


use App\Http\Controllers\Controller;
use App\Services\Reply\UpdateReplyService;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;

class UpdateReplyController extends Controller
{
    protected $_updateReplyService;

    public function __construct(UpdateReplyService $updateReplyService)
    {
        $this->_updateReplyService = $updateReplyService;
    }

    public function updateReplyById(Request $request, $reply_id)
    {
        $data = $request->only(['content']);
        $result = ['status' => 200];

        try {
            $result['data'] = $this->_updateReplyService->updateReplyById($data, $reply_id, $request);
        } catch (\Exception $error) {

            if ($error instanceof UnauthorizedException) {
                $result403 = ['status' => 403, 'error' => 'Unauthorized Operation'];
                return response()->json($result403, $result403['status']);
            }
            $result = ['status' => 500, 'error' => $error->getMessage()];
        }
        return response()->json($result, $result['status']);
    }
}
