<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Post\CreatePostController;
use App\Http\Controllers\Post\DeletePostController;
use App\Http\Controllers\Post\GetPostController;
use App\Http\Controllers\Post\UpdatePostController;
use App\Http\Controllers\Reply\CreateReplyController;
use App\Http\Controllers\Reply\DeleteReplyController;
use App\Http\Controllers\Reply\GetReplyController;
use App\Http\Controllers\Reply\UpdateReplyController;
use App\Http\Controllers\UserRegistrationController;
use Fruitcake\Cors\HandleCors;
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => [HandleCors::class]], function () {
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/user', [UserRegistrationController::class, 'createUser']);
});

Route::group(['middleware' => ['auth:api', HandleCors::class]], function () {
    Route::get('/users/{user_id}/posts', [GetPostController::class, 'getPostsByUserId']);
    Route::post('/posts', [CreatePostController::class, 'createPost']);
    Route::delete('/posts/{post_id}', [DeletePostController::class, 'deletePost']);
    Route::put('/posts/{post_id}', [UpdatePostController::class, 'updatePost']);

    Route::get('/posts/{post_id}/replies', [GetReplyController::class, 'getPrincipalRepliesByPostId']);
    Route::get('/replies/{reply_id}', [GetReplyController::class, 'getChildRepliesByReplyId']);
    Route::post('/reply-post/{post_id}', [CreateReplyController::class, 'createReplyFromPost']);
    Route::post('/replies', [CreateReplyController::class, 'createReplyFromReply']);
    Route::delete('/replies/{reply_id}', [DeleteReplyController::class, 'deleteReplyById']);
    Route::put('/replies/{reply_id}', [UpdateReplyController::class, 'updateReplyById']);
});

